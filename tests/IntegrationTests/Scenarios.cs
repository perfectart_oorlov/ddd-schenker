﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EventFlow;
using EventFlow.Aggregates;
using EventFlow.Configuration;
using EventFlow.Core;
using EventFlow.Extensions;
using EventFlow.Logs;
using EventFlow.TestHelpers;
using FluentAssertions.Extensions;
using NUnit.Framework;
using Schenker.Shipping.Application;
using Schenker.Shipping.Domain.Model.CargoModel.ValueObjects;
using Schenker.Shipping.Domain.Model.LocationModel;
using Schenker.Shipping.Domain.Model.VoyageModel;
using Schenker.Shipping.Domain.Model.VoyageModel.Commands;
using Schenker.Shipping.Queries.InMemory;

namespace Schenker.Shipping.Tests.IntegrationTests
{
    [Category(Categories.Integration)]
    public sealed class Scenarios : Test
    {
        private IRootResolver _resolver;
        private IAggregateStore _aggregateStore;
        private ICommandBus _commandBus;

        [SetUp]
        public void SetUp()
        {
            _resolver = EventFlowOptions.New
            .ConfigureShippingDomain()
            .ConfigureShippingQueriesInMemory()
            .CreateResolver();

            _aggregateStore = _resolver.Resolve<IAggregateStore>();
            _commandBus     = _resolver.Resolve<ICommandBus>();
        }

        [TearDown]
        public void TearDown()
        {
            _resolver.DisposeSafe(new ConsoleLog(), String.Empty);
        }

        [Test]
        public async Task Simple()
        {
            await CreateLocationAggregatesAsync().ConfigureAwait(false);
            await CreateVoyageAggregatesAsync().ConfigureAwait(false);

            var route = new Route(
                Locations.Tokyo,
                Locations.Helsinki,
                1.October(2008).At(11, 00),
                6.November(2008).At(12, 00)
            );

            var booking = _resolver.Resolve<IBookingApplicationService>();
            await booking.BookCargoAsync(route, CancellationToken.None).ConfigureAwait(false);

            var voyage = _resolver.Resolve<IScheduleApplicationService>();
            await voyage.DelayScheduleAsync(
                Voyages.DallasToHelsinkiId,
                TimeSpan.FromDays(7),
                CancellationToken.None
            ).ConfigureAwait(false);
        }

        public Task CreateVoyageAggregatesAsync()
        {
            return Task.WhenAll(Voyages.GetVoyages().Select(CreateVoyageAggregateAsync));
        }

        public Task CreateVoyageAggregateAsync(Voyage voyage)
        {
            return _commandBus.PublishAsync(new VoyageCreateCommand(voyage.Id, voyage.Schedule), CancellationToken.None);
        }

        public Task CreateLocationAggregatesAsync()
        {
            return Task.WhenAll(Locations.GetLocations().Select(CreateLocationAggregateAsync));
        }

        public Task CreateLocationAggregateAsync(Location location)
        {
            return UpdateAsync<LocationAggregate, LocationId>(location.Id, a => a.Create(location.Name));
        }

        private async Task UpdateAsync<TAggregate, TIdentity>(TIdentity id, Action<TAggregate> action)
            where TAggregate : IAggregateRoot<TIdentity>
            where TIdentity : IIdentity
        {
            await _aggregateStore.UpdateAsync<TAggregate, TIdentity>(
                id,
                SourceId.New,
                (a, c) =>
                    {
                        action(a);
                        return Task.FromResult(0);
                    },
                CancellationToken.None
            ).ConfigureAwait(false);
        }
    }
}
