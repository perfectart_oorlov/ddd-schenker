Install the .NET Core utility for the SonarQube scanner:
```bash
dotnet tool install --global dotnet-sonarscanner
```

Start the SonarQube scan job:
```bash
dotnet sonarscanner begin
/d:sonar.host.url=http://localhost:9000
/d:sonar.sources=.
/d:sonar.projectKey=ddd:main
/d:sonar.login=27e8285a1b4d7d3255798ab5d7b75210198939e5
```

Start the SonarQube Docker-instance:
```bash
docker run -d --name sonarqube -p 9000:9000 sonarqube
```
