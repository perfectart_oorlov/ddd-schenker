﻿using System;
using System.Diagnostics;
using System.Threading;

namespace EventFlow.TestHelpers.Extensions
{
    internal static class ProcessExtensions
    {
        public static bool WaitForOutput(
            this Process process,
            string output,
            Action<Process> initialize)
        {
            var autoResetEvent = new AutoResetEvent(false);

            void Handler(object sender, DataReceivedEventArgs args)
            {
                if (args?.Data != null && args.Data.Contains(output))
                    autoResetEvent.Set();
            }

            process.OutputDataReceived += Handler;
            initialize(process);

            var foundOutput = autoResetEvent.WaitOne(TimeSpan.FromSeconds(30));
            process.OutputDataReceived -= Handler;
            return foundOutput;
        }
    }
}
