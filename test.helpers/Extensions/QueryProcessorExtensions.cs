﻿using System.Threading;
using System.Threading.Tasks;
using EventFlow.Queries;

namespace EventFlow.TestHelpers.Extensions
{
    public static class QueryProcessorExtensions
    {
        public static Task<TResult> ProcessAsync<TResult>(
            this IQueryProcessor queryProcessor,
            IQuery<TResult> query)
        {
            return queryProcessor.ProcessAsync(query, CancellationToken.None);
        }
    }
}
