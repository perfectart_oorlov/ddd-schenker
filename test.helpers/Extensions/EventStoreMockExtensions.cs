﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using EventFlow.Aggregates;
using EventFlow.EventStores;
using EventFlow.TestHelpers.Aggregates;
using Moq;

namespace EventFlow.TestHelpers.Extensions
{
    public static class EventStoreMockExtensions
    {
        public static void Arrange_LoadEventsAsync(
            this Mock<IEventStore> eventStoreMock,
            params IDomainEvent<ThingyAggregate, ThingyId>[] domainEvents)
        {
            eventStoreMock
                .Setup(s => s.LoadEventsAsync<ThingyAggregate, ThingyId>(
                    It.IsAny<ThingyId>(),
                    It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult<IReadOnlyCollection<IDomainEvent<ThingyAggregate, ThingyId>>>(domainEvents));

            eventStoreMock
                .Setup(s => s.LoadEventsAsync<ThingyAggregate, ThingyId>(
                    It.IsAny<ThingyId>(),
                    It.IsAny<int>(),
                    It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult<IReadOnlyCollection<IDomainEvent<ThingyAggregate, ThingyId>>>(domainEvents));
        }
    }
}
