﻿using System.Threading;
using System.Threading.Tasks;
using EventFlow.Aggregates;
using EventFlow.Aggregates.ExecutionResults;
using EventFlow.Commands;
using EventFlow.Core;

namespace EventFlow.TestHelpers.Extensions
{
    public static class CommandBusExtensions
    {
        public static Task<TExecutionResult> PublishAsync<TAggregate, TIdentity, TExecutionResult>(
            this ICommandBus commandBus,
            ICommand<TAggregate, TIdentity, TExecutionResult> command)
            where TAggregate : IAggregateRoot<TIdentity>
            where TIdentity : IIdentity
            where TExecutionResult : IExecutionResult
        {
            return commandBus.PublishAsync(command, CancellationToken.None);
        }
    }
}
