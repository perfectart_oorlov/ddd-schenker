﻿using System.Threading;
using System.Threading.Tasks;
using EventFlow.Aggregates;
using EventFlow.Core;

namespace EventFlow.TestHelpers.Extensions
{
    public static class AggregateStoreExtensions
    {
        public static Task<TAggregate> LoadAsync<TAggregate, TIdentity>(
            this IAggregateStore aggregateStore,
            TIdentity id)
            where TAggregate : IAggregateRoot<TIdentity>
            where TIdentity : IIdentity
        {
            return aggregateStore.LoadAsync<TAggregate, TIdentity>(id, CancellationToken.None);
        }
    }
}
