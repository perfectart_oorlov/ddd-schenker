﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;

namespace EventFlow.TestHelpers
{
    public class TcpHelper
    {
        private static readonly Random Random = new Random();

        public static int GetFreePort()
        {
            var ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
            var usedPorts = Enumerable.Empty<int>()
                .Concat(ipGlobalProperties.GetActiveTcpListeners().Select(l => l.Port))
                .Concat(ipGlobalProperties.GetActiveUdpListeners().Select(l => l.Port))
                .Concat(ipGlobalProperties.GetActiveTcpConnections().Select(c => c.LocalEndPoint.Port))
                .Distinct();

            var portLookup = new HashSet<int>(usedPorts);

            while (true)
            {
                var port = Random.Next(10000, 60000);
                if (!portLookup.Contains(port))
                {
                    return port;
                }
            }
        }
    }
}
