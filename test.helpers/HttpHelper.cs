﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EventFlow.TestHelpers
{
    public static class HttpHelper
    {
        private static readonly HttpClient HttpClient = new HttpClient();

        public static async Task<string> GetAsync(Uri uri)
        {
            LogHelper.Log.Information($"GET {uri}");

            using (var httpResponseMessage = await HttpClient.GetAsync(uri).ConfigureAwait(false))
            {
                var content = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

                if (content.Length < 1024)
                    LogHelper.Log.Information($"RESPONSE FROM {uri}{Environment.NewLine}{content}");

                return content;
            }
        }

        public static async Task DeleteAsync(Uri uri)
        {
            LogHelper.Log.Information($"DELETE {uri}");

            using (var httpResponseMessage = await HttpClient.DeleteAsync(uri).ConfigureAwait(false))
            {
                var content = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

                if (content.Length < 1024)
                    LogHelper.Log.Information($"RESPONSE FROM {uri}{Environment.NewLine}{content}");

                httpResponseMessage.EnsureSuccessStatusCode();
            }
        }

        public static async Task<T> GetAsAsync<T>(Uri uri)
        {
            var json = await GetAsync(uri).ConfigureAwait(false);
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
