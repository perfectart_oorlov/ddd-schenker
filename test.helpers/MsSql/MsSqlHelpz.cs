﻿using System;
using System.Collections.Generic;

namespace EventFlow.TestHelpers.MsSql
{
    public static class MsSqlHelpz
    {
        public static IMsSqlDatabase CreateDatabase(string label, bool dropOnDispose = true)
        {
            var connectionString = CreateConnectionString(label);
            var masterConnectionString = connectionString.NewConnectionString("master");
            var sql = $"CREATE DATABASE [{connectionString.Database}]";
            masterConnectionString.Execute(sql);
            return new MsSqlDatabase(connectionString, dropOnDispose);
        }

        public static MsSqlConnectionString CreateConnectionString(string label)
        {
            var databaseName = $"{label}_{DateTime.Now:yyyy-MM-dd-HH-mm}_{Guid.NewGuid():N}";
            var connectionstringParts = new List<string> { $"Database={databaseName}" };
            var environmentServer     = Environment.GetEnvironmentVariable("HELPZ_MSSQL_SERVER");
            var environmentPassword   = Environment.GetEnvironmentVariable("HELPZ_MSSQL_PASS");
            var envrionmentUsername   = Environment.GetEnvironmentVariable("HELPZ_MSSQL_USER");

            connectionstringParts.Add(string.IsNullOrEmpty(environmentServer)
                ? @"Server=."
                : $"Server={environmentServer}");

            connectionstringParts.Add(string.IsNullOrEmpty(envrionmentUsername)
                ? @"Integrated Security=True"
                : $"User Id={envrionmentUsername}");

            connectionstringParts.Add("Connection Timeout=60");

            if (!string.IsNullOrEmpty(environmentPassword))
                connectionstringParts.Add($"Password={environmentPassword}");

            return new MsSqlConnectionString(string.Join(";", connectionstringParts));
        }
    }
}
