﻿using System;
using System.Data.SqlClient;

namespace EventFlow.TestHelpers.MsSql
{
    public interface IMsSqlDatabase : IDisposable
    {
        MsSqlConnectionString ConnectionString { get; }
        bool DropOnDispose { get; }
        void Ping();
        T WithConnection<T>(Func<SqlConnection, T> action);
        void Execute(string sql);
        void WithConnection(Action<SqlConnection> action);
    }
}
