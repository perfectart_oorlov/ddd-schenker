﻿using System;
using System.Data.SqlClient;

namespace EventFlow.TestHelpers.MsSql
{
    public class MsSqlDatabase : IMsSqlDatabase
    {
        public MsSqlDatabase(MsSqlConnectionString connectionString, bool dropOnDispose)
        {
            ConnectionString = connectionString;
            DropOnDispose = dropOnDispose;
            ConnectionString.Ping();
        }

        public MsSqlConnectionString ConnectionString { get; }
        public bool DropOnDispose { get; }

        public void Dispose()
        {
            if (DropOnDispose)
            {
                var masterConnectionString = ConnectionString.NewConnectionString("master");
                masterConnectionString.Execute($"ALTER DATABASE [{ConnectionString.Database}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;");
                masterConnectionString.Execute($"DROP DATABASE [{ConnectionString.Database}];");
            }
        }

        public void Ping()
        {
            ConnectionString.Ping();
        }

        public T WithConnection<T>(Func<SqlConnection, T> action)
        {
            return ConnectionString.WithConnection(action);
        }

        public void Execute(string sql)
        {
            ConnectionString.Execute(sql);
        }

        public void WithConnection(Action<SqlConnection> action)
        {
            ConnectionString.WithConnection(action);
        }
    }
}
