﻿using System.Reflection;

namespace EventFlow.TestHelpers
{
    public static class EventFlowTestHelpers
    {
        public static Assembly Assembly { get; } = typeof(EventFlowTestHelpers).Assembly;
    }
}
