﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventFlow.Aggregates;
using EventFlow.Core;
using EventFlow.EventStores;
using EventFlow.Logs;
using EventFlow.TestHelpers.Aggregates;
using EventFlow.TestHelpers.Aggregates.Entities;
using AutoFixture;
using AutoFixture.AutoMoq;
using Moq;
using NUnit.Framework;

namespace EventFlow.TestHelpers
{
    public abstract class Test
    {
        protected IFixture Fixture { get; private set; }
        protected IDomainEventFactory DomainEventFactory;
        protected ILog Log => LogHelper.Log;

        [SetUp]
        public void SetUpTest()
        {
            Fixture = new Fixture().Customize(new AutoMoqCustomization());

            Fixture.Customize<ThingyId>(x => x.FromFactory(() => ThingyId.New));
            Fixture.Customize<ThingyMessageId>(x => x.FromFactory(() => ThingyMessageId.New));
            Fixture.Customize<EventId>(c => c.FromFactory(() => EventId.New));
            Fixture.Customize<Label>(s => s.FromFactory(() => Label.Named($"label-{Guid.NewGuid():D}")));

            DomainEventFactory = new DomainEventFactory();
        }

        protected T A<T>()
        {
            return Fixture.Create<T>();
        }

        protected List<T> Many<T>(int count = 3)
        {
            return Fixture.CreateMany<T>(count).ToList();
        }

        protected T Mock<T>() where T : class
        {
            return new Mock<T>().Object;
        }

        protected T Inject<T>(T instance) where T : class
        {
            Fixture.Inject(instance);
            return instance;
        }

        protected Mock<T> InjectMock<T>(params object[] args) where T : class
        {
            var mock = new Mock<T>(args);
            Fixture.Inject(mock.Object);
            return mock;
        }

        protected IDomainEvent<ThingyAggregate, ThingyId> ADomainEvent<TAggregateEvent>(int aggregateSequenceNumber = 0)
            where TAggregateEvent : IAggregateEvent
        {
            return ToDomainEvent(A<TAggregateEvent>(), aggregateSequenceNumber);
        }

        protected IReadOnlyCollection<IDomainEvent<ThingyAggregate, ThingyId>> ManyDomainEvents<TAggregateEvent>(
            int count = 3) where TAggregateEvent : IAggregateEvent
        {
            return Enumerable.Range(1, count)
            .Select(ADomainEvent<TAggregateEvent>)
            .ToList();
        }

        protected IDomainEvent<ThingyAggregate, ThingyId> ToDomainEvent<TAggregateEvent>(
            TAggregateEvent aggregateEvent,
            int aggregateSequenceNumber = 0)
            where TAggregateEvent : IAggregateEvent
        {
            return ToDomainEvent(A<ThingyId>(), aggregateEvent, aggregateSequenceNumber);
        }

        protected IDomainEvent<ThingyAggregate, ThingyId> ToDomainEvent<TAggregateEvent>(
            ThingyId thingyId,
            TAggregateEvent aggregateEvent,
            int aggregateSequenceNumber = 0)
            where TAggregateEvent : IAggregateEvent
        {
            var metadata = new Metadata
            {
                Timestamp = A<DateTimeOffset>(),
                SourceId  = A<SourceId>(),
                EventId   = A<EventId>(),
            };

            if (aggregateSequenceNumber == 0)
                aggregateSequenceNumber = A<int>();

            return DomainEventFactory.Create<ThingyAggregate, ThingyId>(
                aggregateEvent,
                metadata,
                thingyId,
                aggregateSequenceNumber
            );
        }

        protected Mock<Func<T>> CreateFailingFunction<T>(T result, params Exception[] exceptions)
        {
            var function = new Mock<Func<T>>();
            var exceptionStack = new Stack<Exception>(exceptions.Reverse());
            function.Setup(f => f()).Returns(() =>
            {
                if (exceptionStack.Any())
                    throw exceptionStack.Pop();
    
                return result;
            });

            return function;
        }
    }
}
