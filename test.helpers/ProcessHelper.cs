﻿using System;
using System.Diagnostics;
using System.IO;
using EventFlow.Core;
using EventFlow.Extensions;
using EventFlow.TestHelpers.Extensions;

namespace EventFlow.TestHelpers
{
    public static class ProcessHelper
    {
        public static IDisposable StartExe(
            string exePath,
            string initializationDone,
            params string[] arguments)
        {
            if (string.IsNullOrEmpty(exePath))
                throw new ArgumentNullException(nameof(exePath));

            if (string.IsNullOrEmpty(initializationDone))
                throw new ArgumentNullException(nameof(initializationDone));

            var workingDirectory = Path.GetDirectoryName(exePath);

            if (string.IsNullOrEmpty(workingDirectory))
                throw new ArgumentException($"Could not find directory for '{exePath}'", nameof(exePath));

            LogHelper.Log.Information($"Starting process: '{exePath}' {string.Join(" ", arguments)}");

            var process = new Process
            {
                StartInfo = new ProcessStartInfo(exePath, string.Join(" ", arguments))
                {
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    WorkingDirectory = workingDirectory,
                }
            };

            var exeName = Path.GetFileName(exePath);

            void OutHandler(object sender, DataReceivedEventArgs e)
            {
                if (!string.IsNullOrEmpty(e.Data))
                    LogHelper.Log.Information($"OUT - {exeName}: {e.Data}");
            }

            void ErrHandler(object sender, DataReceivedEventArgs e)
            {
                if (!string.IsNullOrEmpty(e.Data))
                    LogHelper.Log.Error($"ERR - {exeName}: {e.Data}");
            }

            void InitializeProcess(Process p)
            {
                LogHelper.Log.Information($"{exeName} START =======================================");
                p.Start();
                p.BeginOutputReadLine();
                p.BeginErrorReadLine();
            }

            process.OutputDataReceived += OutHandler;
            process.ErrorDataReceived += ErrHandler;
            process.WaitForOutput(initializationDone, InitializeProcess);

            return new DisposableAction(() =>
                {
                    try
                    {
                        process.OutputDataReceived -= OutHandler;
                        process.ErrorDataReceived -= ErrHandler;
                    }
                    catch (Exception e)
                    {
                        LogHelper.Log.Error($"Failed to kill process: {e.Message}");
                    }
                    finally
                    {
                        process.DisposeSafe("Process");
                    }
                });
        }
    }
}
