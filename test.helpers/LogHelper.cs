﻿using System;
using EventFlow.Logs;

namespace EventFlow.TestHelpers
{
    public static class LogHelper
    {
        private static readonly Lazy<ILog> LazyLog = new Lazy<ILog>(() => new ConsoleLog());
        public static ILog Log => LazyLog.Value;
    }
}
