﻿using System.Threading;
using System.Threading.Tasks;
using EventFlow.Queries;
using EventFlow.Sagas;
using EventFlow.TestHelpers.Aggregates;
using EventFlow.TestHelpers.Aggregates.Commands;
using EventFlow.TestHelpers.Aggregates.Queries;
using FluentAssertions;
using NUnit.Framework;

namespace EventFlow.TestHelpers.Suites
{
    public abstract class IntegrationTestSuiteForServiceRegistration : IntegrationTest
    {
        [Test]
        public async Task PublishingStartAndCompleteTiggerEventsCompletesSaga()
        {
            // Arrange
            var thingyId = A<ThingyId>();

            // Act
            using (var scope = Resolver.BeginScope())
            {
                var commandBus = scope.Resolve<ICommandBus>();
                await commandBus.PublishAsync(new ThingyRequestSagaStartCommand(thingyId), CancellationToken.None).ConfigureAwait(false);
            }
            using (var scope = Resolver.BeginScope())
            {
                var commandBus = scope.Resolve<ICommandBus>();
                await commandBus.PublishAsync(new ThingyRequestSagaCompleteCommand(thingyId), CancellationToken.None).ConfigureAwait(false);
            }

            // Assert
            var thingySaga = await LoadSagaAsync(thingyId).ConfigureAwait(false);
            thingySaga.State.Should().Be(SagaState.Completed);
        }

        [Test]
        public virtual async Task QueryingUsesScopedDbContext()
        {
            using (var scope = Resolver.BeginScope())
            {
                var dbContext = scope.Resolve<IScopedContext>();
                var queryProcessor = scope.Resolve<IQueryProcessor>();

                var result = await queryProcessor.ProcessAsync(new DbContextQuery(), CancellationToken.None);
                result.Should().Be(dbContext.Id);
            }
        }
    }
}
