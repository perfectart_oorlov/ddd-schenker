﻿using System;
using System.Threading;
using System.Threading.Tasks;
using EventFlow.Core.Caching;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace EventFlow.TestHelpers.Suites
{
    public abstract class TestSuiteForInMemoryCache<TSut> : TestsFor<TSut>
        where TSut : IMemoryCache
    {
        [Test]
        public async Task InvokesFactoryAndReturnsValue()
        {
            // Arrange
            var value = A<object>();
            var factory = CreateFactoryMethod(value);

            // Act
            var cacheValue = await Sut.GetOrAddAsync(
                A<CacheKey>(),
                DateTimeOffset.Now.AddDays(1),
                factory.Object,
                CancellationToken.None)
                .ConfigureAwait(false);

            // Assert
            cacheValue.Should().BeSameAs(value);
            factory.Verify(m => m(It.IsAny<CancellationToken>()), Times.Once());
        }

        [Test]
        public void FaultyFactoryMethodThrowsException()
        {
            // Arrange
            var exception = A<Exception>();
            var faultyFactory = CreateFaultyFactoryMethod<object>(exception);

            // Act
            var thrownException = Assert.ThrowsAsync<Exception>(async () => await Sut.GetOrAddAsync(
                A<CacheKey>(),
                DateTimeOffset.Now.AddDays(1),
                faultyFactory.Object,
                CancellationToken.None));

            // Assert
            faultyFactory.Verify(m => m(It.IsAny<CancellationToken>()), Times.Once());
            thrownException.Should().BeSameAs(exception);
        }

        [Test]
        public void FactoryReturningNullThrowsException()
        {
            // Arrange
            var factory = CreateFactoryMethod<object>(null);

            // Act
            var thrownException = Assert.ThrowsAsync<InvalidOperationException>(async () => await Sut.GetOrAddAsync(
                A<CacheKey>(),
                DateTimeOffset.Now.AddDays(1),
                factory.Object,
                CancellationToken.None));

            // Assert
            thrownException.Message.Should().Contain("must not return 'null");
        }

        private static Mock<Func<CancellationToken, Task<T>>> CreateFactoryMethod<T>(T value)
        {
            var mock = new Mock<Func<CancellationToken, Task<T>>>();
            mock
                .Setup(m => m(It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult(value));
            return mock;
        }

        private static Mock<Func<CancellationToken, Task<T>>> CreateFaultyFactoryMethod<T>(Exception exception)
        {
            var mock = new Mock<Func<CancellationToken, Task<T>>>();
            mock
                .Setup(m => m(It.IsAny<CancellationToken>()))
                .Returns(() => { throw exception; });
            return mock;
        }
    }
}