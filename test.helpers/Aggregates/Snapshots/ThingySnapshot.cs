﻿using System.Collections.Generic;
using System.Linq;
using EventFlow.Snapshots;
using EventFlow.TestHelpers.Aggregates.ValueObjects;

namespace EventFlow.TestHelpers.Aggregates.Snapshots
{
    [SnapshotVersion("thingy", 3)]
    public class ThingySnapshot : ISnapshot
    {
        public ThingySnapshot(
            IEnumerable<PingId> pingsReceived,
            IEnumerable<ThingySnapshotVersion> previousVersions)
        {
            PingsReceived    = (pingsReceived ?? Enumerable.Empty<PingId>()).ToList();
            PreviousVersions = (previousVersions ?? Enumerable.Empty<ThingySnapshotVersion>()).ToList();
        }

        public IReadOnlyCollection<PingId> PingsReceived { get; }
        public IReadOnlyCollection<ThingySnapshotVersion> PreviousVersions { get; }
    }
}
