﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EventFlow.Snapshots;

namespace EventFlow.TestHelpers.Aggregates.Snapshots.Upgraders
{
    public class ThingySnapshotV2ToV3Upgrader : ISnapshotUpgrader<ThingySnapshotV2, ThingySnapshot>
    {
        public Task<ThingySnapshot> UpgradeAsync(ThingySnapshotV2 fromVersionedType, CancellationToken cancellationToken)
        {
            var thingySnapshot = new ThingySnapshot(
                fromVersionedType.PingsReceived,
                new [] {ThingySnapshotVersion.Version2, }.Concat(fromVersionedType.PreviousVersions)
            );

            return Task.FromResult(thingySnapshot);
        }
    }
}
