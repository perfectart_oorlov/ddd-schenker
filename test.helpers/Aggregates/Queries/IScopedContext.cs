﻿namespace EventFlow.TestHelpers.Aggregates.Queries
{
    public interface IScopedContext
    {
        string Id { get; }
    }
}
