﻿using EventFlow.Queries;

namespace EventFlow.TestHelpers.Aggregates.Queries
{
    public class ThingyGetQuery : IQuery<Thingy>
    {
        public ThingyGetQuery(ThingyId thingyId)
        {
            ThingyId = thingyId;
        }

        public ThingyId ThingyId { get; }
    }
}
