﻿using System.Threading;
using System.Threading.Tasks;
using EventFlow.Queries;

namespace EventFlow.TestHelpers.Aggregates.Queries
{
    public class DbContextQueryHandler : IQueryHandler<DbContextQuery, string>
    {
        private readonly IScopedContext _scopedContext;

        public DbContextQueryHandler(IScopedContext scopedContext)
        {
            _scopedContext = scopedContext;
        }

        public Task<string> ExecuteQueryAsync(DbContextQuery query, CancellationToken cancellationToken)
        {
            return Task.FromResult(_scopedContext.Id);
        }
    }
}
