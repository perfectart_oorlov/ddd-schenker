﻿using EventFlow.Queries;

namespace EventFlow.TestHelpers.Aggregates.Queries
{
    public class ThingyGetVersionQuery : IQuery<long?>
    {
        public ThingyGetVersionQuery(ThingyId thingyId)
        {
            ThingyId = thingyId;
        }

        public ThingyId ThingyId { get; }
    }
}
