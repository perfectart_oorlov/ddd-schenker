﻿using System.Collections.Generic;
using EventFlow.Queries;
using EventFlow.TestHelpers.Aggregates.Entities;

namespace EventFlow.TestHelpers.Aggregates.Queries
{
    public class ThingyGetMessagesQuery : IQuery<IReadOnlyCollection<ThingyMessage>>
    {
        public ThingyGetMessagesQuery(ThingyId thingyId)
        {
            ThingyId = thingyId;
        }

        public ThingyId ThingyId { get; }
    }
}
