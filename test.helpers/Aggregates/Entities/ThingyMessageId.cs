﻿

using EventFlow.Core;

namespace EventFlow.TestHelpers.Aggregates.Entities
{
    public class ThingyMessageId : Identity<ThingyMessageId>
    {
        public ThingyMessageId(string value) : base(value)
        {
        }
    }
}