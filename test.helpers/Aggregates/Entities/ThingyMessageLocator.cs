﻿

using System.Collections.Generic;
using EventFlow.Aggregates;
using EventFlow.ReadStores;
using EventFlow.TestHelpers.Aggregates.Events;

namespace EventFlow.TestHelpers.Aggregates.Entities
{
    public class ThingyMessageLocator : IReadModelLocator
    {
        public IEnumerable<string> GetReadModelIds(IDomainEvent domainEvent)
        {
            var aggregateEvent = domainEvent.GetAggregateEvent();
            switch (aggregateEvent)
            {
                case ThingyMessageAddedEvent messageAddedEvent:
                    yield return messageAddedEvent.ThingyMessage.Id.Value;
                    break;

                case ThingyMessageHistoryAddedEvent messageHistoryAddedEvent:
                    foreach (var message in messageHistoryAddedEvent.ThingyMessages)
                    {
                        yield return message.Id.Value;
                    }
                    break;
            }
        }
    }
}