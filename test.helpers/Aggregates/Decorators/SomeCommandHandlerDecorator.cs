﻿using System.Threading;
using System.Threading.Tasks;
using EventFlow.Aggregates;
using EventFlow.Aggregates.ExecutionResults;
using EventFlow.Commands;
using EventFlow.Core;

namespace EventFlow.TestHelpers.Aggregates.Decorators
{
    public class SomeCommandHandlerDecorator<TAggregate, TIdentity, TResult, TCommand> :
        CommandHandler<TAggregate, TIdentity, TResult, TCommand>
        where TAggregate : IAggregateRoot<TIdentity>
        where TIdentity : IIdentity
        where TResult : IExecutionResult
        where TCommand : ICommand<TAggregate, TIdentity, TResult>
    {
        public SomeCommandHandlerDecorator(ICommandHandler<TAggregate, TIdentity, TResult, TCommand> inner)
        {
            
        }

        public override Task<TResult> ExecuteCommandAsync(TAggregate aggregate, TCommand command, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }
    }
}
