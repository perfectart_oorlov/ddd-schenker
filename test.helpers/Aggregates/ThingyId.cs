﻿using EventFlow.Core;

namespace EventFlow.TestHelpers.Aggregates
{
    public class ThingyId : Identity<ThingyId>
    {
        public ThingyId(string value) : base(value) { }
    }
}
