﻿

using EventFlow.Aggregates;

namespace EventFlow.TestHelpers.Aggregates.Events
{
    public class ThingySagaCompleteRequestedEvent : AggregateEvent<ThingyAggregate, ThingyId>
    {
    }
}