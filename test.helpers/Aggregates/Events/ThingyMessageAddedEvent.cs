﻿

using EventFlow.Aggregates;
using EventFlow.TestHelpers.Aggregates.Entities;

namespace EventFlow.TestHelpers.Aggregates.Events
{
    public class ThingyMessageAddedEvent : AggregateEvent<ThingyAggregate, ThingyId>
    {
        public ThingyMessageAddedEvent(
            ThingyMessage thingyMessage)
        {
            ThingyMessage = thingyMessage;
        }

        public ThingyMessage ThingyMessage { get; }
    }
}