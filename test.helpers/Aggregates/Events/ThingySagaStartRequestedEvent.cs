﻿

using EventFlow.Aggregates;

namespace EventFlow.TestHelpers.Aggregates.Events
{
    public class ThingySagaStartRequestedEvent : AggregateEvent<ThingyAggregate, ThingyId>
    {
    }
}