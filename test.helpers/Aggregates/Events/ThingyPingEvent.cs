﻿

using EventFlow.Aggregates;
using EventFlow.EventStores;
using EventFlow.TestHelpers.Aggregates.ValueObjects;

namespace EventFlow.TestHelpers.Aggregates.Events
{
    [EventVersion("ThingyPing", 1)]
    public class ThingyPingEvent : AggregateEvent<ThingyAggregate, ThingyId>
    {
        public PingId PingId { get; private set; }

        public ThingyPingEvent(PingId pingId)
        {
            PingId = pingId;
        }
    }
}