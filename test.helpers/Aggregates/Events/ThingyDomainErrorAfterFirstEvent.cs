﻿

using EventFlow.Aggregates;
using EventFlow.EventStores;

namespace EventFlow.TestHelpers.Aggregates.Events
{
    [EventVersion("ThingyDomainErrorAfterFirst", 1)]
    public class ThingyDomainErrorAfterFirstEvent : AggregateEvent<ThingyAggregate, ThingyId>
    {
    }
}