﻿

using EventFlow.Aggregates;

namespace EventFlow.TestHelpers.Aggregates.Sagas.Events
{
    public class ThingySagaCompletedEvent : AggregateEvent<ThingySaga, ThingySagaId>
    {
    }
}