﻿using EventFlow.Aggregates;
using EventFlow.TestHelpers.Aggregates.ValueObjects;

namespace EventFlow.TestHelpers.Aggregates.Sagas.Events
{
    public class ThingySagaPingReceivedEvent : AggregateEvent<ThingySaga, ThingySagaId>
    {
        public ThingySagaPingReceivedEvent(PingId pingId)
        {
            PingId = pingId;
        }

        public PingId PingId { get; }
    }
}
