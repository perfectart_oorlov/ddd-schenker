﻿using EventFlow.Sagas;
using EventFlow.ValueObjects;

namespace EventFlow.TestHelpers.Aggregates.Sagas
{
    public class ThingySagaId : SingleValueObject<string>, ISagaId
    {
        public ThingySagaId(string value) : base(value) { }
    }
}
