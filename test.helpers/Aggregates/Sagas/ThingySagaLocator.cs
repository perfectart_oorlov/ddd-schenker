﻿using System.Threading;
using System.Threading.Tasks;
using EventFlow.Aggregates;
using EventFlow.Sagas;

namespace EventFlow.TestHelpers.Aggregates.Sagas
{
    public class ThingySagaLocator : ISagaLocator
    {
        public Task<ISagaId> LocateSagaAsync(IDomainEvent domainEvent, CancellationToken cancellationToken)
        {
            var aggregateId = domainEvent.GetIdentity();
            return Task.FromResult<ISagaId>(new ThingySagaId($"saga-{aggregateId.Value}"));
        }
    }
}
