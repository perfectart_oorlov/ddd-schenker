﻿

using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EventFlow.Commands;
using EventFlow.TestHelpers.Aggregates.Entities;
using EventFlow.TestHelpers.Aggregates.ValueObjects;

namespace EventFlow.TestHelpers.Aggregates.Commands
{
    public class ThingyImportCommand : Command<ThingyAggregate, ThingyId>
    {
        public IReadOnlyCollection<PingId> PingIds { get; }
        public IReadOnlyCollection<ThingyMessage> ThingyMessages { get; }

        public ThingyImportCommand(
            ThingyId aggregateId,
            IEnumerable<PingId> pingIds,
            IEnumerable<ThingyMessage> thingyMessages)
            : base(aggregateId)
        {
            PingIds = pingIds.ToList();
            ThingyMessages = thingyMessages.ToList();
        }
    }

    public class ThingyImportCommandHandler : CommandHandler<ThingyAggregate, ThingyId, ThingyImportCommand>
    {
        public override Task ExecuteAsync(
            ThingyAggregate aggregate,
            ThingyImportCommand command,
            CancellationToken cancellationToken)
        {
            foreach (var pingId in command.PingIds)
            {
                aggregate.Ping(pingId);
            }

            foreach (var thingyMessage in command.ThingyMessages)
            {
                aggregate.AddMessage(thingyMessage);
            }

            return Task.FromResult(0);
        }
    }
}