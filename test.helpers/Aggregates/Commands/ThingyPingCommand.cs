﻿

using System.Threading;
using System.Threading.Tasks;
using EventFlow.Commands;
using EventFlow.Core;
using EventFlow.TestHelpers.Aggregates.ValueObjects;
using Newtonsoft.Json;

namespace EventFlow.TestHelpers.Aggregates.Commands
{
    [CommandVersion("ThingyPing", 1)]
    public class ThingyPingCommand : Command<ThingyAggregate, ThingyId>
    {
        public PingId PingId { get; }

        public ThingyPingCommand(ThingyId aggregateId, PingId pingId)
            : this(aggregateId, CommandId.New, pingId)
        {
        }

        public ThingyPingCommand(ThingyId aggregateId, ISourceId sourceId, PingId pingId)
            : base (aggregateId, sourceId)
        {
            PingId = pingId;
        }

        [JsonConstructor]
        public ThingyPingCommand(ThingyId aggregateId, SourceId sourceId, PingId pingId)
            : base(aggregateId, sourceId)
        {
            PingId = pingId;
        }
    }

    public class ThingyPingCommandHandler : CommandHandler<ThingyAggregate, ThingyId, ThingyPingCommand>
    {
        public override Task ExecuteAsync(ThingyAggregate aggregate, ThingyPingCommand command, CancellationToken cancellationToken)
        {
            aggregate.Ping(command.PingId);
            return Task.FromResult(0);
        }
    }
}