﻿

using System.Threading;
using System.Threading.Tasks;
using EventFlow.Commands;

namespace EventFlow.TestHelpers.Aggregates.Commands
{
    public class ThingyRequestSagaStartCommand : Command<ThingyAggregate, ThingyId>
    {
        public ThingyRequestSagaStartCommand(ThingyId aggregateId) : base(aggregateId)
        {
        }
    }

    public class ThingyRequestSagaStartCommandHandler : CommandHandler<ThingyAggregate, ThingyId, ThingyRequestSagaStartCommand>
    {
        public override Task ExecuteAsync(ThingyAggregate aggregate, ThingyRequestSagaStartCommand command, CancellationToken cancellationToken)
        {
            aggregate.RequestSagaStart();
            return Task.FromResult(0);
        }
    }
}