﻿

using System.Threading;
using System.Threading.Tasks;
using EventFlow.Aggregates.ExecutionResults;
using EventFlow.Commands;
using EventFlow.TestHelpers.Aggregates.ValueObjects;
using Newtonsoft.Json;

namespace EventFlow.TestHelpers.Aggregates.Commands
{
    [CommandVersion("ThingyMaybePing", 1)]
    public class ThingyMaybePingCommand : Command<ThingyAggregate, ThingyId, IExecutionResult>
    {
        public PingId PingId { get; }
        public bool IsSuccess { get; }

        [JsonConstructor]
        public ThingyMaybePingCommand(ThingyId aggregateId, PingId pingId, bool isSuccess)
            : base(aggregateId, CommandId.New)
        {
            PingId = pingId;
            IsSuccess = isSuccess;
        }
    }

    public class ThingyMaybePingCommandHandler :
        CommandHandler<ThingyAggregate, ThingyId, IExecutionResult, ThingyMaybePingCommand>
    {
        public override Task<IExecutionResult> ExecuteCommandAsync(
            ThingyAggregate aggregate,
            ThingyMaybePingCommand command,
            CancellationToken cancellationToken)
        {
            var executionResult = aggregate.PingMaybe(command.PingId, command.IsSuccess);
            return Task.FromResult(executionResult);
        }
    }
}