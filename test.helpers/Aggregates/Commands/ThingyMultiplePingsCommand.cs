﻿

using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EventFlow.Commands;
using EventFlow.Core;
using EventFlow.TestHelpers.Aggregates.ValueObjects;
using Newtonsoft.Json;

namespace EventFlow.TestHelpers.Aggregates.Commands
{
    [CommandVersion("ThingyMultiplePings", 1)]
    public class ThingyMultiplePingsCommand : Command<ThingyAggregate, ThingyId>
    {
        public IReadOnlyCollection<PingId> PingIds { get; }

        public ThingyMultiplePingsCommand(ThingyId aggregateId, IEnumerable<PingId> pingIds)
            : this(aggregateId, CommandId.New, pingIds)
        {
        }

        public ThingyMultiplePingsCommand(ThingyId aggregateId, ISourceId sourceId, IEnumerable<PingId> pingIds)
            : base (aggregateId, sourceId)
        {
            PingIds = pingIds.ToList();
        }

        [JsonConstructor]
        public ThingyMultiplePingsCommand(ThingyId aggregateId, SourceId sourceId, IEnumerable<PingId> pingIds)
            : base(aggregateId, sourceId)
        {
            PingIds = pingIds.ToList();
        }
    }

    public class ThingyMultiplePingsCommandHandler : CommandHandler<ThingyAggregate, ThingyId, ThingyMultiplePingsCommand>
    {
        public override Task ExecuteAsync(ThingyAggregate aggregate, ThingyMultiplePingsCommand command, CancellationToken cancellationToken)
        {
            foreach (var pingId in command.PingIds)
            {
                aggregate.Ping(pingId);
            }
            return Task.FromResult(0);
        }
    }
}