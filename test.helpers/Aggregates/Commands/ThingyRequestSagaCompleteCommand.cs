﻿

using System.Threading;
using System.Threading.Tasks;
using EventFlow.Commands;

namespace EventFlow.TestHelpers.Aggregates.Commands
{
    public class ThingyRequestSagaCompleteCommand : Command<ThingyAggregate, ThingyId>
    {
        public ThingyRequestSagaCompleteCommand(ThingyId aggregateId) : base(aggregateId)
        {
        }
    }

    public class ThingyRequestSagaCompleteCommandHandler : CommandHandler<ThingyAggregate, ThingyId, ThingyRequestSagaCompleteCommand>
    {
        public override Task ExecuteAsync(ThingyAggregate aggregate, ThingyRequestSagaCompleteCommand command, CancellationToken cancellationToken)
        {
            aggregate.RequestSagaComplete();
            return Task.FromResult(0);
        }
    }
}