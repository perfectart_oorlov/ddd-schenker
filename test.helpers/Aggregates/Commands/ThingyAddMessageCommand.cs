﻿

using System.Threading;
using System.Threading.Tasks;
using EventFlow.Commands;
using EventFlow.TestHelpers.Aggregates.Entities;

namespace EventFlow.TestHelpers.Aggregates.Commands
{
    public class ThingyAddMessageCommand : Command<ThingyAggregate, ThingyId>
    {
        public ThingyMessage ThingyMessage { get; }

        public ThingyAddMessageCommand(
            ThingyId aggregateId,
            ThingyMessage thingyMessage)
            : base(aggregateId)
        {
            ThingyMessage = thingyMessage;
        }
    }

    public class ThingyAddMessageCommandHandler : CommandHandler<ThingyAggregate, ThingyId, ThingyAddMessageCommand>
    {
        public override Task ExecuteAsync(ThingyAggregate aggregate, ThingyAddMessageCommand command, CancellationToken cancellationToken)
        {
            aggregate.AddMessage(command.ThingyMessage);
            return Task.FromResult(0);
        }
    }
}