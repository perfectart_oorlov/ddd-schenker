﻿

using System.Threading;
using System.Threading.Tasks;
using EventFlow.Commands;

namespace EventFlow.TestHelpers.Aggregates.Commands
{
    /// <summary>
    /// "Nop" is short for "no operation"
    /// </summary>
    public class ThingyNopCommand : Command<ThingyAggregate, ThingyId>
    {
        public ThingyNopCommand(ThingyId aggregateId) : base(aggregateId)
        {
        }
    }

    public class ThingyNopCommandHandler : CommandHandler<ThingyAggregate, ThingyId, ThingyNopCommand>
    {
        public override Task ExecuteAsync(ThingyAggregate aggregate, ThingyNopCommand command, CancellationToken cancellationToken)
        {
            return Task.FromResult(0);
        }
    }
}