﻿

using System.Threading;
using System.Threading.Tasks;
using EventFlow.Commands;

namespace EventFlow.TestHelpers.Aggregates.Commands
{
    public class ThingyDomainErrorAfterFirstCommand : Command<ThingyAggregate, ThingyId>
    {
        public ThingyDomainErrorAfterFirstCommand(ThingyId aggregateId) : base(aggregateId) { }
    }

    public class ThingyDomainErrorAfterFirstCommandHander : CommandHandler<ThingyAggregate, ThingyId, ThingyDomainErrorAfterFirstCommand>
    {
        public override Task ExecuteAsync(ThingyAggregate aggregate, ThingyDomainErrorAfterFirstCommand command, CancellationToken cancellationToken)
        {
            aggregate.DomainErrorAfterFirst();
            return Task.FromResult(0);
        }
    }
}