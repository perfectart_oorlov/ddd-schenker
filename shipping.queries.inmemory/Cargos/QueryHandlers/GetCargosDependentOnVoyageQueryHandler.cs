﻿using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using EventFlow.Queries;
using EventFlow.ReadStores.InMemory;
using Schenker.Shipping.Domain.Model.CargoModel;
using Schenker.Shipping.Domain.Model.CargoModel.Queries;

namespace Schenker.Shipping.Queries.InMemory.Cargos.QueryHandlers
{
    public class GetCargosDependentOnVoyageQueryHandler
        : IQueryHandler<GetCargosDependentOnVoyageQuery, IReadOnlyCollection<Cargo>>
    {
        private readonly IInMemoryReadStore<CargoReadModel> _readStore;

        public GetCargosDependentOnVoyageQueryHandler(IInMemoryReadStore<CargoReadModel> readStore)
        {
            _readStore = readStore;
        }

        public async Task<IReadOnlyCollection<Cargo>> ExecuteQueryAsync(
            GetCargosDependentOnVoyageQuery query,
            CancellationToken cancellationToken)
        {
            var cargoReadModels = await _readStore.FindAsync(
                rm => rm.DependentVoyageIds.Contains(query.VoyageId),
                cancellationToken
            ).ConfigureAwait(false);

            return cargoReadModels.Select(rm => rm.ToCargo()).ToList();
        }
    }
}
