﻿using System.Reflection;
using EventFlow;
using EventFlow.Extensions;
using Schenker.Shipping.Queries.InMemory.Cargos;
using Schenker.Shipping.Queries.InMemory.Voyage;

namespace Schenker.Shipping.Queries.InMemory
{
    public static class SchenkerShippingQueriesInMemory
    {
        public static Assembly Assembly { get; } = typeof(SchenkerShippingQueriesInMemory).Assembly;

        public static IEventFlowOptions ConfigureShippingQueriesInMemory(this IEventFlowOptions eventFlowOptions)
        {
            return eventFlowOptions
            .AddQueryHandlers(Assembly)
            .UseInMemoryReadStoreFor<VoyageReadModel>()
            .UseInMemoryReadStoreFor<CargoReadModel>();
        }
    }
}
