﻿using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using EventFlow.Queries;
using EventFlow.ReadStores.InMemory;
using Schenker.Shipping.Domain.Model.VoyageModel.Queries;

namespace Schenker.Shipping.Queries.InMemory.Voyage.QueryHandlers
{
    public class GetAllVoyagesQueryHandler :
        IQueryHandler<GetAllVoyagesQuery, IReadOnlyCollection<Domain.Model.VoyageModel.Voyage>>
    {
        private readonly IInMemoryReadStore<VoyageReadModel> _readStore;

        public GetAllVoyagesQueryHandler(IInMemoryReadStore<VoyageReadModel> readStore)
        {
            _readStore = readStore;
        }

        public async Task<IReadOnlyCollection<Domain.Model.VoyageModel.Voyage>> ExecuteQueryAsync(GetAllVoyagesQuery query, CancellationToken cancellationToken)
        {
            var voyageReadModels = await _readStore.FindAsync(rm => true, cancellationToken).ConfigureAwait(false);
            return voyageReadModels.Select(rm => rm.ToVoyage()).ToList();
        }
    }
}
