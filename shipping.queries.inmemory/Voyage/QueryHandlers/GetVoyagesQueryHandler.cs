﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EventFlow.Queries;
using EventFlow.ReadStores.InMemory;
using Schenker.Shipping.Domain.Model.VoyageModel;
using Schenker.Shipping.Domain.Model.VoyageModel.Queries;

namespace Schenker.Shipping.Queries.InMemory.Voyage.QueryHandlers
{
    public class GetVoyagesQueryHandler
        : IQueryHandler<GetVoyagesQuery, IReadOnlyCollection<Domain.Model.VoyageModel.Voyage>>
    {
        private readonly IInMemoryReadStore<VoyageReadModel> _readStore;

        public GetVoyagesQueryHandler(IInMemoryReadStore<VoyageReadModel> readStore)
        {
            _readStore = readStore;
        }

        public async Task<IReadOnlyCollection<Domain.Model.VoyageModel.Voyage>> ExecuteQueryAsync(GetVoyagesQuery query, CancellationToken cancellationToken)
        {
            var voyageIds = new HashSet<VoyageId>(query.VoyageIds);
            var voyageReadModels = await _readStore.FindAsync(
                rm => voyageIds.Contains(rm.Id),
                cancellationToken
            ).ConfigureAwait(false);

            return voyageReadModels.Select(rm => rm.ToVoyage()).ToList();
        }
    }
}
