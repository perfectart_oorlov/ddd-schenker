﻿using EventFlow.Aggregates;
using EventFlow.ReadStores;
using Schenker.Shipping.Domain.Model.VoyageModel;
using Schenker.Shipping.Domain.Model.VoyageModel.Events;
using Schenker.Shipping.Domain.Model.VoyageModel.ValueObjects;

namespace Schenker.Shipping.Queries.InMemory.Voyage
{
    public class VoyageReadModel : IReadModel,
        IAmReadModelFor<VoyageAggregate, VoyageId, VoyageCreatedEvent>,
        IAmReadModelFor<VoyageAggregate, VoyageId, VoyageScheduleUpdatedEvent>
    {
        public VoyageId Id { get; private set; }
        public Schedule Schedule { get; private set; }

        public void Apply(IReadModelContext context, IDomainEvent<VoyageAggregate, VoyageId, VoyageCreatedEvent> e)
        {
            Id       = e.AggregateIdentity;
            Schedule = e.AggregateEvent.Schedule;
        }

        public void Apply(
            IReadModelContext context,
            IDomainEvent<VoyageAggregate, VoyageId, VoyageScheduleUpdatedEvent> domainEvent)
        {
            Schedule = domainEvent.AggregateEvent.Schedule;
        }

        public Domain.Model.VoyageModel.Voyage ToVoyage()
        {
            return new Domain.Model.VoyageModel.Voyage(
                Id,
                Schedule
            );
        }
    }
}
