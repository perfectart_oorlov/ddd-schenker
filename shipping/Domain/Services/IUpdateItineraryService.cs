﻿using System.Threading;
using System.Threading.Tasks;
using Schenker.Shipping.Domain.Model.CargoModel.ValueObjects;

namespace Schenker.Shipping.Domain.Services
{
    public interface IUpdateItineraryService
    {
        Task<Itinerary> UpdateItineraryAsync(Itinerary itinerary, CancellationToken cancellationToken);
    }
}
