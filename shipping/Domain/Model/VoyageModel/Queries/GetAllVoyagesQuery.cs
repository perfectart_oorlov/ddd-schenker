﻿using System.Collections.Generic;
using EventFlow.Queries;

namespace Schenker.Shipping.Domain.Model.VoyageModel.Queries
{
    public sealed class GetAllVoyagesQuery : IQuery<IReadOnlyCollection<Voyage>> { }
}
