﻿using System.Linq;
using System.Collections.Generic;
using EventFlow.Queries;

namespace Schenker.Shipping.Domain.Model.VoyageModel.Queries
{
    public sealed class GetVoyagesQuery : IQuery<IReadOnlyCollection<Voyage>>
    {
        public GetVoyagesQuery(IEnumerable<VoyageId> voyageIds)
        {
            VoyageIds = voyageIds.ToList();
        }

        public IReadOnlyCollection<VoyageId> VoyageIds { get; }
    }
}
