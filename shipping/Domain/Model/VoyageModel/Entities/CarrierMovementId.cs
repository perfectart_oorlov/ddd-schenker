﻿using EventFlow.Core;

namespace Schenker.Shipping.Domain.Model.VoyageModel.Entities
{
    public sealed class CarrierMovementId : Identity<CarrierMovementId>
    {
        public CarrierMovementId(string value) : base(value) { }
    }
}
