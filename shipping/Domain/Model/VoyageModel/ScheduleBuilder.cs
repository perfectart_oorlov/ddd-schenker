﻿using System;
using System.Collections.Generic;
using Schenker.Shipping.Domain.Model.LocationModel;
using Schenker.Shipping.Domain.Model.VoyageModel.Entities;
using Schenker.Shipping.Domain.Model.VoyageModel.ValueObjects;

namespace Schenker.Shipping.Domain.Model.VoyageModel
{
    public sealed class ScheduleBuilder
    {
        private readonly List<CarrierMovement> _carrierMovements = new List<CarrierMovement>();
        private LocationId _departureLocation;

        public ScheduleBuilder(LocationId departureLocation)
        {
            _departureLocation = departureLocation;
        }

        public ScheduleBuilder Add(
            LocationId arrivalLocationId,
            DateTimeOffset departureTime,
            DateTimeOffset arrivalTime)
        {
            _carrierMovements.Add(new CarrierMovement(
                CarrierMovementId.New,
                _departureLocation,
                arrivalLocationId,
                departureTime,
                arrivalTime)
            );

            _departureLocation = arrivalLocationId;
            return this;
        }

        public Schedule Build()
        {
            return new Schedule(_carrierMovements);
        }
    }
}
