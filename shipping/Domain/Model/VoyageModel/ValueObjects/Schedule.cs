﻿using System;
using System.Collections.Generic;
using System.Linq;
using Schenker.Shipping.Domain.Model.VoyageModel.Entities;
using EventFlow.ValueObjects;

namespace Schenker.Shipping.Domain.Model.VoyageModel.ValueObjects
{
    public sealed class Schedule : ValueObject
    {
        public Schedule(IEnumerable<CarrierMovement> carrierMovements)
        {
            var carrierMovementList = (carrierMovements ?? Enumerable.Empty<CarrierMovement>()).ToList();

            if (!carrierMovementList.Any())
                throw new ArgumentException(nameof(carrierMovements));

            CarrierMovements = carrierMovementList;
        }

        public IReadOnlyList<CarrierMovement> CarrierMovements { get; }

        public Schedule Delay(TimeSpan delay)
        {
            var carrierMovements = CarrierMovements
                .Select(m => new CarrierMovement(
                    m.Id,
                    m.DepartureLocationId,
                    m.ArrivalLocationId,
                    m.DepartureTime + delay,
                    m.ArrivalTime + delay
                )
            );

            return new Schedule(carrierMovements);
        }
    }
}
