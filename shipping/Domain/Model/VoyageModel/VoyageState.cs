﻿using EventFlow.Aggregates;
using Schenker.Shipping.Domain.Model.VoyageModel.Events;
using Schenker.Shipping.Domain.Model.VoyageModel.ValueObjects;

namespace Schenker.Shipping.Domain.Model.VoyageModel
{
    public sealed class VoyageState : AggregateState<VoyageAggregate, VoyageId, VoyageState>,
        IApply<VoyageCreatedEvent>,
        IApply<VoyageScheduleUpdatedEvent>
    {
        public Schedule Schedule { get; private set; }

        public void Apply(VoyageCreatedEvent aggregateEvent)
        {
            Schedule = aggregateEvent.Schedule;
        }

        public void Apply(VoyageScheduleUpdatedEvent aggregateEvent)
        {
            Schedule = aggregateEvent.Schedule;
        }
    }
}
