﻿using System.Threading;
using System.Threading.Tasks;
using EventFlow.Commands;
using Schenker.Shipping.Domain.Model.VoyageModel.ValueObjects;

namespace Schenker.Shipping.Domain.Model.VoyageModel.Commands
{
    public sealed class VoyageCreateCommand : Command<VoyageAggregate, VoyageId>
    {
        public VoyageCreateCommand(VoyageId aggregateId, Schedule schedule) : base(aggregateId)
        {
            Schedule = schedule;
        }

        public Schedule Schedule { get; }
    }

    public sealed class VoyageCreateCommandHandler : CommandHandler<VoyageAggregate, VoyageId, VoyageCreateCommand>
    {
        public override Task ExecuteAsync(
            VoyageAggregate aggregate,
            VoyageCreateCommand command,
            CancellationToken cancellationToken)
        {
            aggregate.Create(command.Schedule);
            return Task.FromResult(0);
        }
    }
}
