﻿using EventFlow.Aggregates;
using EventFlow.EventStores;
using Schenker.Shipping.Domain.Model.VoyageModel.ValueObjects;

namespace Schenker.Shipping.Domain.Model.VoyageModel.Events
{
    [EventVersion("VoyageDelayed", 1)]
    public sealed class VoyageScheduleUpdatedEvent : AggregateEvent<VoyageAggregate, VoyageId>
    {
        public VoyageScheduleUpdatedEvent(Schedule schedule)
        {
            Schedule = schedule;
        }

        public Schedule Schedule { get; }
    }
}
