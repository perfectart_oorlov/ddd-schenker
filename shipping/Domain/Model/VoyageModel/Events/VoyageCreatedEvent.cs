﻿using EventFlow.Aggregates;
using EventFlow.EventStores;
using Schenker.Shipping.Domain.Model.VoyageModel.ValueObjects;

namespace Schenker.Shipping.Domain.Model.VoyageModel.Events
{
    [EventVersion("VoyageCreated", 1)]
    public sealed class VoyageCreatedEvent : AggregateEvent<VoyageAggregate, VoyageId>
    {
        public VoyageCreatedEvent(Schedule schedule)
        {
            Schedule = schedule;
        }

        public Schedule Schedule { get; }
    }
}
