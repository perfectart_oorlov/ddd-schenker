﻿using System;
using EventFlow.Aggregates;
using EventFlow.Extensions;
using Schenker.Shipping.Domain.Model.VoyageModel.Events;
using Schenker.Shipping.Domain.Model.VoyageModel.ValueObjects;

namespace Schenker.Shipping.Domain.Model.VoyageModel
{
    public sealed class VoyageAggregate : AggregateRoot<VoyageAggregate, VoyageId>
    {
        private readonly VoyageState _state = new VoyageState();

        public VoyageAggregate(VoyageId id) : base(id)
        {
            Register(_state);
        }

        public Schedule Schedule => _state.Schedule;

        public void Create(Schedule schedule)
        {
            Specs.AggregateIsNew.ThrowDomainErrorIfNotSatisfied(this);
            Emit(new VoyageCreatedEvent(schedule));
        }

        public void Delay(TimeSpan delay)
        {
            Specs.AggregateIsCreated.ThrowDomainErrorIfNotSatisfied(this);

            if (delay == TimeSpan.Zero)
                return;

            var delayedSchedule = Schedule.Delay(delay);
            Emit(new VoyageScheduleUpdatedEvent(delayedSchedule));
        }
    }
}
