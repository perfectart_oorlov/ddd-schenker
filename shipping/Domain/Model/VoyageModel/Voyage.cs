﻿using System;
using EventFlow.Entities;
using Schenker.Shipping.Domain.Model.VoyageModel.ValueObjects;

namespace Schenker.Shipping.Domain.Model.VoyageModel
{
    public sealed class Voyage : Entity<VoyageId>
    {
        public Voyage(VoyageId id, Schedule schedule) : base(id)
        {
            if (schedule == null)
                throw new ArgumentNullException(nameof(schedule));

            Schedule = schedule;
        }

        public Schedule Schedule { get; }
    }
}
