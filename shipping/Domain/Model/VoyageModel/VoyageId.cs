﻿using EventFlow.Core;
using EventFlow.ValueObjects;
using Newtonsoft.Json;

namespace Schenker.Shipping.Domain.Model.VoyageModel
{
    [JsonConverter(typeof(SingleValueObjectConverter))]
    public sealed class VoyageId : SingleValueObject<string>, IIdentity
    {
        public VoyageId(string value) : base(value) { }
    }
}
