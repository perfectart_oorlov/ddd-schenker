﻿using EventFlow.Aggregates;
using EventFlow.EventStores;

namespace Schenker.Shipping.Domain.Model.LocationModel.Events
{
    [EventVersion("LocationCreated", 1)]
    public sealed class LocationCreatedEvent : AggregateEvent<LocationAggregate, LocationId>
    {
        public LocationCreatedEvent(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}
