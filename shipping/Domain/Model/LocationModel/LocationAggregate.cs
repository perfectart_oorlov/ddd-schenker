﻿using EventFlow.Aggregates;
using EventFlow.Exceptions;
using Schenker.Shipping.Domain.Model.LocationModel.Events;

namespace Schenker.Shipping.Domain.Model.LocationModel
{
    public sealed class LocationAggregate : AggregateRoot<LocationAggregate, LocationId>
    {
        private readonly LocationState _state = new LocationState();

        public LocationAggregate(LocationId id) : base(id)
        {
            Register(_state);
        }

        public void Create(string name)
        {
            if (!IsNew)
                throw DomainError.With("Location is already created");

            Emit(new LocationCreatedEvent(name));
        }
    }
}
