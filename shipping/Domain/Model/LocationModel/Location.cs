﻿using EventFlow.Entities;

namespace Schenker.Shipping.Domain.Model.LocationModel
{
    public sealed class Location : Entity<LocationId>
    {
        public Location(LocationId id, string name) : base(id)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}
