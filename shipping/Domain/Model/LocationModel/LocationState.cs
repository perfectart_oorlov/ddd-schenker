﻿using EventFlow.Aggregates;
using Schenker.Shipping.Domain.Model.LocationModel.Events;

namespace Schenker.Shipping.Domain.Model.LocationModel
{
    public sealed class LocationState
        : AggregateState<LocationAggregate, LocationId, LocationState>, IApply<LocationCreatedEvent>
    {
        public string Name { get; private set; }

        public void Apply(LocationCreatedEvent aggregateEvent)
        {
            Name = aggregateEvent.Name;
        }
    }
}
