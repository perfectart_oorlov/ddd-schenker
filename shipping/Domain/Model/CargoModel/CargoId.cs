﻿using EventFlow.Core;
using EventFlow.ValueObjects;
using Newtonsoft.Json;

namespace Schenker.Shipping.Domain.Model.CargoModel
{
    [JsonConverter(typeof(SingleValueObjectConverter))]
    public sealed class CargoId : Identity<CargoId>
    {
        public CargoId(string value) : base(value) { }
    }
}
