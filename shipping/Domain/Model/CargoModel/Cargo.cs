﻿using EventFlow.Entities;
using Schenker.Shipping.Domain.Model.CargoModel.ValueObjects;

namespace Schenker.Shipping.Domain.Model.CargoModel
{
    public sealed class Cargo : Entity<CargoId>
    {
        public Cargo(CargoId id, Route route, Itinerary itinerary) : base(id)
        {
            Route     = route;
            Itinerary = itinerary;
        }

        public Route Route { get; }

        public Itinerary Itinerary { get; }
    }
}
