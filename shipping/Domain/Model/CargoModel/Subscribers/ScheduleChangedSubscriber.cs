﻿using System.Threading;
using System.Threading.Tasks;
using EventFlow.Aggregates;
using EventFlow.Jobs;
using EventFlow.Subscribers;
using Schenker.Shipping.Domain.Model.CargoModel.Jobs;
using Schenker.Shipping.Domain.Model.VoyageModel;
using Schenker.Shipping.Domain.Model.VoyageModel.Events;

namespace Schenker.Shipping.Domain.Model.CargoModel.Subscribers
{
    public sealed class ScheduleChangedSubscriber
        : ISubscribeSynchronousTo<VoyageAggregate, VoyageId, VoyageScheduleUpdatedEvent>
    {
        private readonly IJobScheduler _jobScheduler;

        public ScheduleChangedSubscriber(IJobScheduler jobScheduler)
        {
            _jobScheduler = jobScheduler;
        }

        public Task HandleAsync(IDomainEvent<VoyageAggregate, VoyageId, VoyageScheduleUpdatedEvent> domainEvent, CancellationToken cancellationToken)
        {
            var job = new VerifyCargosForVoyageJob(domainEvent.AggregateIdentity);
            return _jobScheduler.ScheduleNowAsync(job, cancellationToken);
        }
    }
}
