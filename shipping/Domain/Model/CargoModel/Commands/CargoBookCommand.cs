﻿using System.Threading;
using System.Threading.Tasks;
using EventFlow.Commands;
using Schenker.Shipping.Domain.Model.CargoModel.ValueObjects;

namespace Schenker.Shipping.Domain.Model.CargoModel.Commands
{
    public sealed class CargoBookCommand : Command<CargoAggregate, CargoId>
    {
        public CargoBookCommand(CargoId aggregateId, Route route) : base(aggregateId)
        {
            Route = route;
        }

        public Route Route { get; }
    }

    public sealed class CargoBookCommandHandler : CommandHandler<CargoAggregate, CargoId, CargoBookCommand>
    {
        public override Task ExecuteAsync(
            CargoAggregate aggregate,
            CargoBookCommand command,
            CancellationToken cancellationToken)
        {
            aggregate.Book(command.Route);
            return Task.FromResult(0);
        }
    }
}
