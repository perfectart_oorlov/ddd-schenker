﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventFlow.Extensions;
using EventFlow.ValueObjects;
using Schenker.Shipping.Domain.Model.CargoModel.Entities;
using Schenker.Shipping.Domain.Model.CargoModel.Specifications;
using Schenker.Shipping.Domain.Model.LocationModel;

namespace Schenker.Shipping.Domain.Model.CargoModel.ValueObjects
{
    public sealed class Itinerary : ValueObject
    {
        public Itinerary(IEnumerable<TransportLeg> transportLegs)
        {
            var legsList = (transportLegs ?? Enumerable.Empty<TransportLeg>()).ToList();

            if (!legsList.Any())
                throw new ArgumentException(nameof(transportLegs));

            (new TransportLegsAreConnectedSpecification()).ThrowDomainErrorIfNotSatisfied(legsList);
            TransportLegs = legsList;
        }

        public IReadOnlyList<TransportLeg> TransportLegs { get; }

        public LocationId DepartureLocation()
        {
            return TransportLegs.First().LoadLocation;
        }

        public DateTimeOffset DepartureTime()
        {
            return TransportLegs.First().UnloadTime;
        }

        public DateTimeOffset ArrivalTime()
        {
            return TransportLegs.Last().UnloadTime;
        }

        public LocationId ArrivalLocation()
        {
            return TransportLegs.Last().UnloadLocation;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            return TransportLegs;
        }
    }
}
