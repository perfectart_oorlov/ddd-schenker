﻿using EventFlow.Aggregates;
using Schenker.Shipping.Domain.Model.CargoModel.Events;
using Schenker.Shipping.Domain.Model.CargoModel.ValueObjects;

namespace Schenker.Shipping.Domain.Model.CargoModel
{
    public sealed class CargoState : AggregateState<CargoAggregate, CargoId, CargoState>,
        IApply<CargoBookedEvent>,
        IApply<CargoItinerarySetEvent>
    {
        public Route Route { get; private set; }
        public Itinerary Itinerary { get; private set; }

        public void Apply(CargoBookedEvent aggregateEvent)
        {
            Route = aggregateEvent.Route;
        }

        public void Apply(CargoItinerarySetEvent aggregateEvent)
        {
            Itinerary = aggregateEvent.Itinerary;
        }
    }
}
