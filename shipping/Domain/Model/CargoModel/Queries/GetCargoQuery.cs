﻿using System.Collections.Generic;
using System.Linq;
using EventFlow.Queries;

namespace Schenker.Shipping.Domain.Model.CargoModel.Queries
{
    public sealed class GetCargosQuery : IQuery<IReadOnlyCollection<Cargo>>
    {
        public GetCargosQuery(params CargoId[] cargoIds) : this((IEnumerable<CargoId>) cargoIds) { }

        public GetCargosQuery(IEnumerable<CargoId> cargoIds)
        {
            CargoIds = cargoIds.ToList();
        }

        public IReadOnlyCollection<CargoId> CargoIds { get; }
    }
}
