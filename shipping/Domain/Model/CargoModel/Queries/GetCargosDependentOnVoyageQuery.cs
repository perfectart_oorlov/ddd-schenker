﻿using System.Collections.Generic;
using EventFlow.Queries;
using Schenker.Shipping.Domain.Model.VoyageModel;

namespace Schenker.Shipping.Domain.Model.CargoModel.Queries
{
    public sealed class GetCargosDependentOnVoyageQuery : IQuery<IReadOnlyCollection<Cargo>>
    {
        public GetCargosDependentOnVoyageQuery(VoyageId voyageId)
        {
            VoyageId = voyageId;
        }

        public VoyageId VoyageId { get; }
    }
}
