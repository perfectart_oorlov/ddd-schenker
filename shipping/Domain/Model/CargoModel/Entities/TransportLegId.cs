﻿using EventFlow.Core;
using EventFlow.ValueObjects;
using Newtonsoft.Json;

namespace Schenker.Shipping.Domain.Model.CargoModel.Entities
{
    [JsonConverter(typeof(SingleValueObjectConverter))]
    public sealed class TransportLegId : Identity<TransportLegId>
    {
        public TransportLegId(string value) : base(value) { }
    }
}
