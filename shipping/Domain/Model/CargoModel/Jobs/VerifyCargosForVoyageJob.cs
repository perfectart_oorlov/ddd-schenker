﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EventFlow.Configuration;
using EventFlow.Jobs;
using EventFlow.Queries;
using Schenker.Shipping.Domain.Model.CargoModel.Queries;
using Schenker.Shipping.Domain.Model.VoyageModel;

namespace Schenker.Shipping.Domain.Model.CargoModel.Jobs
{
    [JobVersion("VerifyCargosForVoyage", 1)]
    public sealed class VerifyCargosForVoyageJob : IJob
    {
        public VoyageId VoyageId { get; }

        public VerifyCargosForVoyageJob(VoyageId voyageId)
        {
            VoyageId = voyageId;
        }

        public async Task ExecuteAsync(IResolver resolver, CancellationToken cancellationToken)
        {
            var queryProcessor = resolver.Resolve<IQueryProcessor>();
            var jobScheduler   = resolver.Resolve<IJobScheduler>();
            var cargos         = await queryProcessor.ProcessAsync(new GetCargosDependentOnVoyageQuery(VoyageId), cancellationToken).ConfigureAwait(false);
            var jobs           = cargos.Select(x => new VerifyCargoItineraryJob(x.Id));
            await Task.WhenAll(jobs.Select(x => jobScheduler.ScheduleNowAsync(x, cancellationToken))).ConfigureAwait(false);
        }
    }
}
