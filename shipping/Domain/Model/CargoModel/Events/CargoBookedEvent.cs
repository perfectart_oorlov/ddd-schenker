﻿using EventFlow.Aggregates;
using EventFlow.EventStores;
using Schenker.Shipping.Domain.Model.CargoModel.ValueObjects;

namespace Schenker.Shipping.Domain.Model.CargoModel.Events
{
    [EventVersion("CargoBooked", 1)]
    public sealed class CargoBookedEvent : AggregateEvent<CargoAggregate, CargoId>
    {
        public CargoBookedEvent(Route route)
        {
            Route = route;
        }

        public Route Route { get; }
    }
}
