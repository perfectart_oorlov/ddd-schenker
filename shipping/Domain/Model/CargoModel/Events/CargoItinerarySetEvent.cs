﻿using EventFlow.Aggregates;
using EventFlow.EventStores;
using Schenker.Shipping.Domain.Model.CargoModel.ValueObjects;

namespace Schenker.Shipping.Domain.Model.CargoModel.Events
{
    [EventVersion("CargoItinerarySet", 1)]
    public sealed class CargoItinerarySetEvent : AggregateEvent<CargoAggregate, CargoId>
    {
        public CargoItinerarySetEvent(Itinerary itinerary)
        {
            Itinerary = itinerary;
        }

        public Itinerary Itinerary { get; }
    }
}
