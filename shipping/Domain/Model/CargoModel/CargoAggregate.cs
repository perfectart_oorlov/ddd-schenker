﻿using EventFlow.Aggregates;
using EventFlow.Extensions;
using Schenker.Shipping.Domain.Model.CargoModel.Events;
using Schenker.Shipping.Domain.Model.CargoModel.ValueObjects;

namespace Schenker.Shipping.Domain.Model.CargoModel
{
    public sealed class CargoAggregate : AggregateRoot<CargoAggregate, CargoId>
    {
        private readonly CargoState _state = new CargoState();

        public CargoAggregate(CargoId id) : base(id)
        {
            Register(_state);
        }

        public Route Route => _state.Route;
        public Itinerary Itinerary => _state.Itinerary;

        public void Book(Route route)
        {
            Specs.AggregateIsNew.ThrowDomainErrorIfNotSatisfied(this);
            Emit(new CargoBookedEvent(route));
        }

        public void SetItinerary(Itinerary itinerary)
        {
            Specs.AggregateIsCreated.ThrowDomainErrorIfNotSatisfied(this);
            Route.Specification().ThrowDomainErrorIfNotSatisfied(itinerary);
            Emit(new CargoItinerarySetEvent(itinerary));
        }
    }
}
