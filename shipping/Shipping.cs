﻿using System.Reflection;
using EventFlow;
using EventFlow.Extensions;
using Schenker.Shipping.Application;
using Schenker.Shipping.Domain.Services;
using Schenker.Shipping.ExternalServices.Routing;

namespace Schenker.Shipping
{
    public static class SchenkerShipping
    {
        public static Assembly Assembly { get; } = typeof(SchenkerShipping).Assembly;

        public static IEventFlowOptions ConfigureShippingDomain(this IEventFlowOptions options)
        {
            return options.AddDefaults(Assembly).RegisterServices(x =>
            {
                x.Register<IBookingApplicationService, BookingApplicationService>();
                x.Register<IScheduleApplicationService, ScheduleApplicationService>();
                x.Register<IUpdateItineraryService, UpdateItineraryService>();
                x.Register<IRoutingService, RoutingService>();
            });
        }
    }
}
