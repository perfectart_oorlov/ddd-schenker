﻿using System;
using Schenker.Shipping.Domain.Model.VoyageModel.Entities;

namespace Schenker.Shipping.Extensions
{
    public static class CarrierMovementExtensions
    {
        public static TimeSpan TravelTime(this CarrierMovement carrierMovement)
        {
            return carrierMovement.ArrivalTime - carrierMovement.DepartureTime;
        }
    }
}
