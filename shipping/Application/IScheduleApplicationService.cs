﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Schenker.Shipping.Domain.Model.VoyageModel;

namespace Schenker.Shipping.Application
{
    public interface IScheduleApplicationService
    {
        Task DelayScheduleAsync(VoyageId voyageId, TimeSpan delay, CancellationToken cancellationToken);
    }
}
