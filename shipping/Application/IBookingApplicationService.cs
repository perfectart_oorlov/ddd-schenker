﻿using System.Threading;
using System.Threading.Tasks;
using Schenker.Shipping.Domain.Model.CargoModel;
using Schenker.Shipping.Domain.Model.CargoModel.ValueObjects;

namespace Schenker.Shipping.Application
{
    public interface IBookingApplicationService
    {
        Task<CargoId> BookCargoAsync(Route route, CancellationToken cancellationToken);
    }
}
