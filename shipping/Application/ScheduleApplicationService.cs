﻿using System;
using System.Threading;
using System.Threading.Tasks;
using EventFlow;
using Schenker.Shipping.Domain.Model.VoyageModel;
using Schenker.Shipping.Domain.Model.VoyageModel.Commands;

namespace Schenker.Shipping.Application
{
    public sealed class ScheduleApplicationService : IScheduleApplicationService
    {
        private readonly ICommandBus _commandBus;

        public ScheduleApplicationService(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        public Task DelayScheduleAsync(VoyageId voyageId, TimeSpan delay, CancellationToken cancellationToken)
        {
            return _commandBus.PublishAsync(new VoyageDelayCommand(voyageId, delay), cancellationToken);
        }
    }
}
