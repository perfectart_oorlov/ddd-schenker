﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Schenker.Shipping.Domain.Model.CargoModel.ValueObjects;

namespace Schenker.Shipping.ExternalServices.Routing
{
    public interface IRoutingService
    {
        Task<IReadOnlyCollection<Itinerary>> CalculateItinerariesAsync(Route route, CancellationToken cancellationToken);
    }
}
